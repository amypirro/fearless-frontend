import React, { useEffect, useState } from 'react';

function ConferenceForm({ getConferences }) {
    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const [name, setName] = useState("");
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [starts, setStarts] = useState("");
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const [ends, setEnds] = useState("");
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const [description, setDescription] = useState("");
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const [maxPresentations, setMaxPresentations] = useState("");
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const [maxAttendees, setMaxAttendees] = useState("");
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const [location, setLocation] = useState("");
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const confResponse = await fetch(conferenceUrl, fetchConfig);
        if (confResponse.ok) {
            const newConference = await confResponse.json();

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
            getConferences();

        }
    }

    return (
        <div className="my-4 container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleStartsChange} value={starts} required type="date" name="starts" className="form-control" />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEndsChange} value={ends} required type="date" name="ends" className="form-control" />
                                <label htmlFor="">Ends</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description">Description</label>
                                <textarea onChange={handleDescriptionChange} value={description} required type="text" name="description" className="form-control"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" className="form-control" />
                                <label htmlFor="max_presentations">Maximum presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" className="form-control" />
                                <label htmlFor="max_attendees">Maximum attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;